import {Char} from '../model/char'

export class CharReader{
    public readCharacter(file, fun){
        let reader = new FileReader();
        reader.onload = (ev) =>{
            fun(JSON.parse(reader.result));
        }
        reader.readAsText(file);
    }
    
}

