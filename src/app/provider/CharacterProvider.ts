import { Injectable } from '@angular/core';
import { Char } from '../model/char'

@Injectable()
export class CharacterProvider{

    character : Char;


    public getCharacter() : Char{
        console.log("get character "+this.character);
        return this.character;
    }

    public setCharacter(character : Char) {
        console.log("Setting character");
        this.character = character;
        console.log("current char: "+JSON.stringify(this.character));
    }

}