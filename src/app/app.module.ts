import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CharacterPage } from '../pages/character/character';
import { EditPage } from '../pages/edit/edit';
import { RunlogPage } from '../pages/runlog/runlog';
import { EquipmentPage } from '../pages/equipment/equipment';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {CharacterProvider}  from './provider/CharacterProvider';
import {AktionFilterPipe} from './pipes/AktionFilterPipe';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CharacterPage,
    RunlogPage,
    EquipmentPage,
    EditPage,
    AktionFilterPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CharacterPage,
    EditPage,
    RunlogPage,
    EquipmentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CharacterProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
