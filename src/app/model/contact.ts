export class Contact{
    name : string;
    connection : string;
    loyalty : string;
    type: string;
    description : string;
}