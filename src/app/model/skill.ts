import {Attribute} from './attribute';

export class Skill{
    name : string;
    rank : string;
    dicepool: string;
    linked_attribute: Attribute;
    type : string;
}