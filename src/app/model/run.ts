export class Run {
    date: string;
    title: string;
    characters: string;
    gamemaster: string;
    story: string;
    notes: string;
    runlog: string;
    money: number;
    karma: number;
}