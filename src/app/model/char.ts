import {Attribute} from './attribute';
import {Karma} from './karma';
import {Equipment} from './equipment';
import {Contact} from './contact';
import {Quality} from './quality';
import {Skill} from './skill';
import {Limits} from './limits';
import {Initiative} from './initiative';
import {Run} from './run';

export class Char{
    
    name : string;
    story : string;
    metatype: string;
    height: string;
    weight: string;
    age: string;   
    money : number;
    karma : Karma;
    magic : string;
    resonance : string;
    essence : string;
    edge : string;
    attributes : Attribute[];
    equipment : Equipment[];
    contacts : Contact[];
    qualities : Quality[];
    skills : Skill[];
    limits : Limits;
    initiative : Initiative;
    runs: Run[];

}
 
