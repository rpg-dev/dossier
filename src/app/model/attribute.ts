export class Attribute{
    name : string;
    natural : string;
    augmented : string;
    natural_maximum : string;
}