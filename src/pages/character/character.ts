import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EditPage } from '../edit/edit';
import { CharacterProvider } from '../../app/provider/CharacterProvider'

@Component({
  selector: 'page-character',
  templateUrl: 'character.html'
})
export class CharacterPage {
  constructor(public navCtrl: NavController, public navParams: NavParams,private characterProvider : CharacterProvider) {
  }

  openEdit() {    
    this.navCtrl.push(EditPage);
  }
}
