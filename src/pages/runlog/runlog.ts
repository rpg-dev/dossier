import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Char } from '../../app/model/char';
import { Run } from '../../app/model/run';
import {CharacterProvider} from '../../app/provider/CharacterProvider'

@Component({
  selector: 'page-runlog',
  templateUrl: 'runlog.html'
})
export class RunlogPage {

  public char: Char;
  constructor(public navCtrl: NavController, private characterProvider: CharacterProvider) {
    this.char = characterProvider.getCharacter();
  }

}
