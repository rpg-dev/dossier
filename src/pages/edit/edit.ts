import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CharacterProvider} from '../../app/provider/CharacterProvider';
import {Char} from '../../app/model/char';

@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html'
})
export class EditPage {

  personForm : FormGroup;
  attributeForm : FormGroup;
  limitForm : FormGroup;
  character : Char;

  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder, private charService : CharacterProvider) {
    this.character = charService.getCharacter();
    this.personForm = formBuilder.group({
      name: [this.character.name],
      metatype: [this.character.metatype],
      height: [this.character.height],
      weight:[this.character.weight],
      age:[this.character.age]
    });
    var attributes = {};
    for(let attribute of this.character.attributes){
      if(attribute.name == 'KON'){
        attributes['kon'] = attribute.natural;
      }
      if(attribute.name == 'GES'){
        attributes['ges'] = attribute.natural;
      }
      if(attribute.name == 'REA'){
        attributes['rea'] = attribute.natural;
      }
      if(attribute.name == 'STR'){
        attributes['str'] = attribute.natural;
      }
      if(attribute.name == 'WIL'){
        attributes['wil'] = attribute.natural;
      }
      if(attribute.name == 'LOG'){
        attributes['log'] = attribute.natural;
      }
      if(attribute.name == 'INT'){
        attributes['int'] = attribute.natural;
      }
      if(attribute.name == 'CHA'){
        attributes['cha'] = attribute.natural;
      }
    }
    this.attributeForm = formBuilder.group(attributes);
    this.limitForm = formBuilder.group(this.character.limits);
  }

  public save(){
     console.log(this.personForm.value);
     console.log(this.attributeForm.value);
     console.log(this.limitForm.value);
  }
}
