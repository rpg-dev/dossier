import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import {CharReader} from '../../app/persistence/CharReader'
import {Char} from '../../app/model/char';
import {DomSanitizer, SafeUrl, SafeResourceUrl} from "@angular/platform-browser";
import {CharacterProvider} from '../../app/provider/CharacterProvider';
import { CharacterPage } from '../character/character';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  recentFiles = Array<String>();
  dataStr : SafeResourceUrl | string;
  download : string = "char.json";

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, private storage: Storage, private sanitizer:DomSanitizer, private charService : CharacterProvider) {
    this.storage.get('recent').then((val) => {
      if ( val != null ) {
        this.recentFiles = val;
      }
    });
    if (this.charService.character == null) {
      this.menuCtrl.enable(false);
    }
  }
    

  public loadCharacter = function(file){
    if (! this.recentFiles.includes(file)) {
      this.recentFiles.push(file);
      this.storage.set('recent', this.recentFiles);
    }
    var charReader = new CharReader();


    charReader.readCharacter(file, (char) => {
      this.charService.setCharacter(char);        
      this.createLink();
      this.menuCtrl.enable(true);
      this.navCtrl.push(CharacterPage);
    });
  }
  public exportCharacter = function(event){
    this.createLink();
  }  

  private createLink(){
    this.dataStr = this.sanitizer.bypassSecurityTrustResourceUrl("data:text/json;charset=utf-8,"+encodeURIComponent(JSON.stringify(this.charService.getCharacter())));   
  }
}
