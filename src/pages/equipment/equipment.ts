import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Char } from '../../app/model/char';
import { Equipment } from '../../app/model/equipment';
import {CharacterProvider} from '../../app/provider/CharacterProvider'

@Component({
  selector: 'page-equipment',
  templateUrl: 'equipment.html'
})
export class EquipmentPage {

  public char: Char;
  public equipmentCategories = [];
  constructor(public navCtrl: NavController, private characterProvider : CharacterProvider) {
    this.char = characterProvider.getCharacter();
    var tempCategories = [];
    for (var i=0, len=this.char.equipment.length; i<len; i++) {
      var e = this.char.equipment[i];
      var pos = tempCategories.indexOf(e.category);
      if (pos >= 0) {
        this.equipmentCategories[pos].push(e);
      } else {
        tempCategories.push(e.category);
        this.equipmentCategories.push([e]);
      }
    }
    this.equipmentCategories.sort(function(a,b) {return (a[0].category > b[0].category) ? 1 : (b[0].category > a[0].category) ? -1 : 0});
    console.log(this.equipmentCategories);
  }
}
