import json
from pathlib import Path
from xmljson import badgerfish as bf
from xml.etree.ElementTree import fromstring

INFILE = Path("samplechar.chum5")
OUTFILE = Path(INFILE.stem+".json")
SKILLFILE = Path("skills.xml")
SKILLS = bf.data(fromstring(SKILLFILE.read_text()))

def find_skill(id):
    for skill in SKILLS["chummer"]["skills"]["skill"]:
        if skill["id"]["$"] == id:
            return skill

def build_description(contact):
    role = contact["role"]["$"] if contact["role"] else ""
    location = contact["location"]["$"] if contact["location"] else ""
    return f"{role} {location}"

def build_armormods(armor):
    mods = ""
    if armor["armormods"]:
        mods = " ".join([armormod["name"]["$"] 
        + " " 
        + str(armormod["rating"]["$"] if armormod["rating"]["$"] > 0 else "") 
        for armormod in armor["armormods"]["armormod"]])
    return "Rüstung " + str(armor["armor"]["$"]) + " " + mods

def build_weaponmods(weapon):
    mods = ""
    if "accessories" in weapon:
        mods = " ".join([accessory["name"]["$"] for accessory in weapon["accessories"]["accessory"]])
    return "Schaden " + str(weapon["damage"]["$"]) + " DK " + str(weapon["ap"]["$"]) + " " + str(weapon["ammo"]["$"]) + " Schuss " + str(weapon["accuracy"]["$"]) + " Präzision " + mods

stringdata = INFILE.read_text()
data = bf.data(fromstring(stringdata))
# manually create datastructure needed
character = {
    "name": data["character"]["alias"]["$"] if data["character"]["alias"]["$"] else data["character"]["name"]["$"],
    "metatype": data["character"]["metavariant"]["$"] if data["character"]["metavariant"]["$"] else data["character"]["metatype"]["$"],
    "age": data["character"]["age"]["$"] if data["character"]["age"] else "",
    "height": data["character"]["height"] if data["character"]["height"] else "",
    "weight": data["character"]["weight"] if data["character"]["weight"] else "",
    "essence": "?",
    "limits": {
        "physical": "?",
        "mental": "?",
        "social": "?"
    },
    "initiative": {
        "physical": "?",
        "astral": "?",
        "ar": "?"
    },
    "attributes": [
        {
            "name": attribute["name"]["$"],
            "natural": attribute["totalvalue"]["$"]-attribute["augmodifier"]["$"],
            "augmented": attribute["totalvalue"]["$"],
            "natural_maximum": attribute["metatypemax"]["$"]
        } for attribute in data["character"]["attributes"]["attribute"]
        ],
    "skills": [
        {
            "name": find_skill(skill["suid"]["$"])["name"]["$"],
            "rank": skill["base"]["$"],
            "dicepool": "?",
            "linked_attribute": find_skill(skill["suid"]["$"])["attribute"]["$"],
            "type": "Aktion"
        } for skill in data["character"]["newskills"]["skills"]["skill"] if skill["base"]["$"]>0
    ] + 
    [
        {
            "name": skill["name"]["$"],
            "rank": skill["base"]["$"],
            "dicepool": "?",
            "linked_attribute": "LOG" if skill["type"]["$"] in ["Professional", "Academic"] else "INT",
            "type": "Wissen"
        } for skill in data["character"]["newskills"]["knoskills"]["skill"] if skill["base"]["$"]>0
    ],
    "contacts" : [
        {
            "name": contact["name"]["$"] if contact["name"] else "",
            "connection": contact["connection"]["$"] if contact["connection"] else "",
            "loyality": contact["loyalty"]["$"] if contact["loyalty"] else "",
            "description": build_description(contact)
        } for contact in data["character"]["contacts"]["contact"]
    ],
    "qualities" : [
        {
            "name": quality["name"]["$"] + " " + quality["extra"]["$"] if quality["extra"] else "",
            "type": quality["type"]["$"] if "type" in quality else ""
        } for quality in data["character"]["qualities"]["quality"]
    ],
    "equipment" : [
        {
            "name": power["name"]["$"] +  " " + power["extra"]["$"] if power["extra"] else power["name"]["$"],
            "description": power["rating"]["$"],
            "category": "Adeptenkraft"
        } for power in data["character"]["powers"]["power"]
    ] + [
        {
            "name": armor["name"]["$"],
            "description": build_armormods(armor),
            "category": "Rüstung"
        } for armor in data["character"]["armors"]["armor"]
    ] + [
        {
            "name": weapon["name"]["$"],
            "description": build_weaponmods(weapon),
            "category": "Waffe:"+weapon["category"]["$"]
        } for weapon in data["character"]["weapons"]["weapon"]
    ] + [  # will break with multiple lifestyles
        {
            "name": data["character"]["lifestyles"]["lifestyle"]["name"]["$"],
            "description": data["character"]["lifestyles"]["lifestyle"]["cost"]["$"],
            "category": "Lebensstil"
        }
    ] + [
        {
            "name": gear["name"]["$"],
            "description": data["rating"] if "rating" in data else "",
            "category": "Weiteres"
        } for gear in data["character"]["gears"]["gear"]
    ],
    "runs": []
}

OUTFILE.write_text(json.dumps(character, indent=2))